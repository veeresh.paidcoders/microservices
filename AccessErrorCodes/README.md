# Microservices

This repository contains all the lambda functions, independent programs and algorithms.


AccessSpecificErrorCodes(Access specific Error code based on Errorcode, Manufacturer, Model) 

Local Run 
* First setup the python virtual environment, please refer CONFLUENCE/MICROSERVICES/python virtual environment.
* Move AccessErrorCodes.py and mainCode.py inside Microservices folder, run mainCode.py python mainCode.py



Third-party dependencies: Since lambda does not support many libraries used in our lambda functions, we need to provide them manually. The list of libraries being used are given in dependencies.txt. In order to create a lambda layer, the libraries need to be locally installed and uploaded to AWS Lambda using:

pip install -r dependencies.txt .


Deploy as Lambda function 
* First setup the python virtual environment, please refer CONFLUENCE/MICROSERVICES/python virtual environment.

* Move AccessErrorCodes.py and mainCode.py inside deployment-package, install all the libraries as mentioned in dependencies.txt using pip and zip all the files in one zip file and import it in the newely created lambda function if the size is less than 4 MB else place it in s3 for lambda_function in AWS. 

