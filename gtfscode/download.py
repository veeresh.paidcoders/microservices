import zipfile
import boto3
import io
#from boto3.session import session

import copy
import os

#AWS Access Key id and Secret Key
ACCESS_KEY_ID = ''
SECRET_KEY = ''

session = boto3.session.Session(
    aws_access_key_id=ACCESS_KEY_ID, 
    aws_secret_access_key=SECRET_KEY
)

s3 = session.resource('s3')

bucket = 'gtfsinfo'

my_bucket = s3.Bucket(bucket)
print(my_bucket.objects.all())
for s3_files in my_bucket.objects.all():
    print(s3_files.key)
    
print("Downloading zips from s3 ...")

for s3_files in my_bucket.objects.all():
    my_bucket.download_file(s3_files.key,'/home/veershetty/temp/'+s3_files.key)
