from distutils.dir_util import copy_tree
import os, zipfile
import glob

from gtfsiphi import FeedData
from gtfsiphi.utils import (
    calculateRouteDistance,
    tripsInRoute,
    stopsInRoute,
    depotsForFeed,
)

dir_name = '/home/veershetty/temp'
extension = ".zip"
new_dir_name = '/home/veershetty/extractedzipfolder/'

os.chdir(dir_name) # change directory from working dir to dir with files

for item in os.listdir(dir_name): # loop through items in dir
    if item.endswith(extension): # check for ".zip" extension
        file_name = os.path.abspath(item) # get full path of files
        zip_ref = zipfile.ZipFile(file_name) # create zipfile object
        zip_ref.extractall(new_dir_name+os.path.splitext(str(item))[0]) # extract file to dir
        zip_ref.close() # close file
        #os.remove(file_name) # delete zipped file
        

        # copy subdirectory example
        fromDirectory = new_dir_name+os.path.splitext(str(item))[0]
        toDirectory = "/home/veershetty/anaconda3/lib/python3.8/site-packages/gtfsiphi-0.0.1-py3.8.egg/gtfsiphi/data"

        files = glob.glob('/home/veershetty/anaconda3/lib/python3.8/site-packages/gtfsiphi-0.0.1-py3.8.egg/gtfsiphi/data/*.txt')
        #Delete all *.txt files from the directory
        for f in files:
            os.remove(f)
        #Copy the text files from zip extracted directory to data/ directory 
        copy_tree(fromDirectory, toDirectory)
        feed = FeedData(directory="/home/veershetty/anaconda3/lib/python3.8/site-packages/gtfsiphi-0.0.1-py3.8.egg/gtfsiphi/data/")
        print(feed.depots)
        routes = feed.listRoutes()
        print(routes)
