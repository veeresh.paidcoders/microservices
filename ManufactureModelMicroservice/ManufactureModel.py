from pymongo import MongoClient
from flask import Flask, jsonify, request 
from flask_cors import CORS
import json
import jsons
from bson import json_util
from bson.json_util import dumps

def ManufacturerModelTest(event, context):

    NewManufacturer = event['Manufacturer']
    client = MongoClient("mongodb+srv://test:test@cluster0.gzucq.mongodb.net/test?retryWrites=true&w=majority")
    db = client.get_database('ErrorCodes')
    records = db.ManufactureModel
    if NewManufacturer != "" :
        records1 = dumps(records.find({"Manufacturer": NewManufacturer}))
    else:
        records1 = dumps(records.find({ }))
    
    json_data = json.loads(records1)
    
    return {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
        },
        'body': json_data
    }
