#This is a driver code that invokes the lambda function
import ManufactureModel
import os
import json
#The event object carries the needed parameters to the lambda function
event = ({"Manufacturer": "Signet"})
context = "Test"
print(json.dumps(ManufactureModel.ManufacturerModelTest(event, context)))