#This is a driver code that invokes the lambda function
import AccessAllVendorErrorCodes
import os
import json
#The event object carries the needed parameters to the lambda function
event = ({"ErrorCode":6, "Manufacturer": ""})
context = "Test"
print(json.dumps(AccessAllVendorErrorCodes.ManufacturerModelTest(event, context)))