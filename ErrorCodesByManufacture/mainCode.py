#This is a driver code that invokes the lambda function
import AccessErrorCodesModel
import os
import json
#The event object carries the needed parameters to the lambda function, the user can mention Manufacturer and Model of own choice
event = ({"Manufacturer": "Signet","Model": "Signet Q"})
context = "Test"
print(json.dumps(AccessErrorCodesModel.ManufacturerErrorTest(event, context)))